<?php 
  $pathURL = 'http://'.$_SERVER['HTTP_HOST'].'/cms/';
  include_once 'admin/banco.php'; 
  include_once 'admin/engine/funcoes.php'; 
?>
<html>
<head>
	<!-- 
		Desenvolvido por FFDEVWEB 
		http://ffdevweb.com.br
	 -->
  <title>Home Page</title>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
  <link rel="stylesheet" type="text/css" href="<?php echo $pathURL; ?>bootstrap/css/bootstrap.css">
  <link rel="stylesheet" type="text/css" href="<?php echo $pathURL; ?>css/style.css">
  <script type="text/javascript" src="<?php echo $pathURL; ?>js/jquery.js"></script>
  <script type="text/javascript" src="<?php echo $pathURL; ?>bootstrap/js/bootstrap.js"></script>
</head>
<body>