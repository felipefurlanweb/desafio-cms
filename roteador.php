<?php
    class Roteador {
        protected $uri = array();
        protected $controlador;
        protected $acao;

        public function __construct() {
            $this->parametros();
        }

        public function parametros() {
            $this->uri = ( isset($_GET['par']) ) ? explode('/', $_GET['par']) : array('');
        }

        public function getRequestURI() {
            $ruri = explode('/', $_SERVER['REQUEST_URI']);
            $stack = $ruri;
            array_shift($stack);
            return $stack;
        }

        public function parametro($key) {
            if (array_key_exists($key, $this->uri)) {
                return $this->uri[$key];
            } else {
                return false;
            }
        }

        public function getControlador() {
            $this->controlador = ( $this->uri[0] == NULL ) ? 'index' : $this->uri[0];
            return ( is_string($this->controlador) ) ? $this->controlador : 'index';
        }
        
        public function getAcao() {
            if (isset($this->uri[1]) && strlen($this->uri[1]) != 0 && is_string($this->uri[1])) {
                $this->acao = $this->uri[1];
            } else {
                $this->acao = "index";
            }

            return $this->acao;
        }
    }
?>