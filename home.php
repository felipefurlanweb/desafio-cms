<?php include_once 'header.php'; ?>

  <section>

    <div class="container text-right marginTop marginBottom">
      <a href="admin/" target="_blank" class="btn btn-xs btn-info">Painel Administrativo</a>
    </div>

  </section>

  <section>

    <div class="jumbotron">
      <div class="container">
        <h1>Home Page</h1>
        <p>Projeto CMS</p>
      </div>
    </div>

  </section>

  <section>
    <div class="container">

        <?php

          $query = "SELECT * FROM posts ORDER BY data DESC";
          $query = mysql_query($query);
          $i=0;
          while ($res = mysql_fetch_array($query)) {
            $i++;

            if ($i == 1) {
              echo "<div class='row post'>";
            }

            $id = $res["id"];
            $titulo = $res["titulo"];
            $corpo = $res["corpo"];
            $corpo = substr($corpo, 0, 400).'...';
            $corpo = str_replace("<p>", "", $corpo); 
            $corpo = str_replace("</p>", "", $corpo); 
            $corpo = preg_replace("/<img[^>]+\>/i", "(imagem) <br/>", $corpo); 
            $path = $res["path"];
            $data = $res["data"];
            $data = FormataData_Brasil($data, "tsp");
            if ($path == "") {
              $path = $pathURL.'post/'.$id;
            }else{
              $path = $pathURL.'post/'.$path;
            }

            echo
            '
              <div class="col-xs-12 col-md-4">
                <div class="row">
                  <div class="col-xs-12 col-md-12">
                    <a href="'.$path.'">
                      <h3>'.$titulo.'</h3>
                    </a>
                  </div>
                  <div class="col-xs-12 col-md-12">
                  </div>
                  <div class="col-xs-12 col-md-12 text-justify">
                    <h5>'.$corpo.'</h5>
                  </div>
                  <div class="col-xs-12 col-md-12">
                    <div class="row">
                      <div class="col-xs-6">
                        <small>Data: '.$data.'</small>
                      </div>
                      <div class="col-xs-6 text-right">
                        <a href="'.$path.'" class="btn btn-xs btn-primary">Leia mais</a>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            ';

            if ($i == 3) {
              echo "</div>";
              $i=0;
            }

          } 
        ?>

    </div>
  </section>

<?php include_once 'footer.php'; ?>