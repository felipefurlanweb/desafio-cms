<?php include_once 'header.php'; ?>

  <?php 
    $param = $uri[2];
    $id = $param;
    if (is_numeric($id)) {
      $query = "SELECT * FROM posts WHERE id = $id";
      $query = mysql_query($query);
      $numRows = mysql_num_rows($query);
      if ($numRows == 0) {
        ECHO '<br/><h1 class="text-center">404</h1>';
        ECHO '<br/><h2 class="text-center">Nada encontrado</h2>';
        ECHO '<br/><div class="alert alert-danger text-center"><b>Atenção</b> Post inválido</div><br/>';
        ECHO '<div class="alert alert-warning text-center">Clique <a href="'.$pathURL.'">aqui</a> para voltar</div>';
        die();
      }
    }else{
      $query = "SELECT * FROM posts WHERE path = '$id'";
      $query = mysql_query($query);
      $numRows = mysql_num_rows($query);
      if ($numRows == 0) {
        ECHO '<br/><h1 class="text-center">404</h1>';
        ECHO '<br/><h2 class="text-center">Nada encontrado</h2>';
        ECHO '<br/><div class="alert alert-danger text-center"><b>Atenção</b> Post inválido</div><br/>';
        ECHO '<div class="alert alert-warning text-center">Clique <a href="'.$pathURL.'">aqui</a> para voltar</div>';
        die();
      }
    }

    $res = mysql_fetch_assoc($query);
    $titulo = $res["titulo"];
    $corpo = $res["corpo"];
    $path = $res["path"];
    $data = $res["data"];
  ?>

  <section>

    <div class="jumbotron">
      <div class="container">
        <div class="col-xs-12">
          <h2><?php echo $titulo ?></h2>
        </div>
      </div>
    </div>

  </section>

  <section>

    <div class="container">

      <div class="col-xs-12">
        <ol class="breadcrumb">
          <li><a href="<?php echo $pathURL; ?>">Home Page</a></li>
          <li class="active">Post</li>
        </ol>
      </div>  

    </div>

  </section>

  <section>
    <div class="container">

      <div class="col-xs-12 col-md-12 text-justify">
        <small><?php echo FormataData_Brasil($data, "tsp"); ?></small>
        <hr>
        <?php echo $corpo; ?>
      </div>

    </div>
  </section>

<?php include_once 'footer.php'; ?>