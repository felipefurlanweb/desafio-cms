<?php 
if (!isset($_SESSION["cms"])) {
    session_start();
}
    include_once 'funcoes.php';
    include_once '../banco.php';

    ini_set('display_errors', 1);
    ini_set('display_startup_errors', 1);
    error_reporting(E_ALL);

    $id = $_POST['id'];
    switch ($id) {

        case 'login':
            login();
            break;

        case 'postAdd':
            postAdd();
            break;
        case 'postEdit':
            postEdit();
            break;
        case 'postRemove':
            postRemove();
            break;

        case 'signup':
            signup();
            break;

    }

    // FUNÇÕES

    ///////////////////////////////////////////////////////////////////////////////////////////////

    function signup(){
        $usuario = $_POST["usuario"];
        $senha = $_POST["senha"];
        $senha = md5($senha);

        $query = "SELECT * FROM usuarios WHERE nome = '%s'";
        $query = sprintf($query, addslashes($usuario));
        $query = mysql_query($query);
        $numRows = mysql_num_rows($query);
        if ($numRows > 0) {
            setaMensagem("Já existe um registro com este usuário.", "alert alert-danger");
            goToPage("../login.php");
            die();
        }
        $query = "INSERT INTO usuarios (nome, senha) VALUES ('%s', '%s')";
        $query = sprintf($query, addslashes($usuario), addslashes($senha));
        $query = mysql_query($query);
        if ($query) {
            $_SESSION["cms"]["logado"] = 1;
            goToPage("../index.php");
        }else{
            setaMensagem("Algo deu errado, tente novamente", "alert alert-danger");
            goToPage("../signup.php");
        }
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////

    function login(){
        $nome = $_POST["nome"];
        $senha = $_POST["senha"];
        $senha = md5($senha);
        $query = "SELECT * FROM usuarios WHERE nome = '%s' AND senha = '%s'";
        $query = sprintf($query, addslashes($nome), addslashes($senha));
        $query = mysql_query($query);
        $numRows = mysql_num_rows($query);
        if ($numRows > 0) {
            $_SESSION["cms"]["logado"] = 1;
            goToPage("../index.php");
        }else{
            setaMensagem("Dados inválidos", "alert alert-danger");
            goToPage("../login.php");
        }
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////

    function postAdd(){
        $titulo = $_POST["titulo"];
        $corpo = $_POST["corpo"];
        $path = $_POST["path"];
        $query = "INSERT INTO posts (titulo, corpo, path) VALUES ('%s', '%s', '%s')";
        $query = sprintf($query, addslashes($titulo), addslashes($corpo), addslashes($path));
        $query = mysql_query($query);
        if ($query) {
            setaMensagem("Cadastro realizado com sucesso", "alert alert-success");
            goToPage("../posts.php");
        }else{
            setaMensagem("Algo deu errado, tente novamente", "alert alert-danger");
            goToPage("../posts.php");
        }
    }
    
    function postEdit(){
        $id = $_POST["idModel"];
        $titulo = $_POST["titulo"];
        $corpo = $_POST["corpo"];
        $path = $_POST["path"];
        $query = "UPDATE posts SET titulo = '%s', corpo = '%s', path = '%s' WHERE id = $id";
        $query = sprintf($query, addslashes($titulo), addslashes($corpo), addslashes($path));
        $query = mysql_query($query);
        if ($query) {
            setaMensagem("Cadastro alterado com sucesso", "alert alert-success");
            goToPage("../posts.php");
        }else{
            setaMensagem("Algo deu errado, tente novamente", "alert alert-danger");
            goToPage("../posts.php");
        }
    }

    function postRemove(){
        $id = $_POST["idModel"];
        $query = "DELETE FROM posts WHERE id = $id";
        $query = mysql_query($query);
        if ($query) {
            setaMensagem("Cadastro deletado com sucesso", "alert alert-success");
            echo '1';
        }else{
            setaMensagem("Algo deu errado, tente novamente", "alert alert-danger");
            echo '0';
        }
    }

