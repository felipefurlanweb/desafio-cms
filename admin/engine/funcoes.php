<?php 

function FormataData_Brasil($timestamp, $formato="dl")
{
	// Recebe uma data e hora no formato MySQl (aaaa-mm-dd hh:mm:ss) e retorna o que foi solicitado no $formato:
	// $formato:
	// "dc" - retorna dd/mm/aa
	// "dl" - retorna dd/mm/aaaa
	// "hc" - retorna hh:mm
	// "hl" - retorna hh:mm:ss
	// "ts" - retorna dd/mm/aaaa hh:mm:ss
	
	$timestamp = explode(" ", $timestamp);
	$data = $timestamp[0];
	$hora = $timestamp[1];

	$data_separada = explode("-", $data);
	$hora_separada = explode(":", $hora);
	
	switch($formato)
	{
		case "dc":
			return $data_separada[2]."/".$data_separada[1]."/".substr($data_separada[0], -2, 2);
			break;
			
		case "dl":
			return $data_separada[2]."/".$data_separada[1]."/".$data_separada[0];
			break;

		case "hc":
			return $hora_separada[0].":".$hora_separada[1];
			break;
		
		case "hl":
			return $hora_separada[0].":".$hora_separada[1].":".$hora_separada[2];
			break;

		case "ts":
			return $data_separada[2]."/".$data_separada[1]."/".$data_separada[0]." ".$hora_separada[0].$hora_separada[1].$hora_separada[2];
			break;
		
		case "tsp":
			return $data_separada[2]."/".$data_separada[1]."/".$data_separada[0]." às ".$hora_separada[0].":".$hora_separada[1];
			break;			
					
		default:
			return $timestamp;			
	}
}


function VoltaMensagem($msg, $tipo){

	setaMensagem($msg, $tipo);

	voltaPaginaAnterior();

}


function setaMensagem($msg, $tipo){

	if (!isset($_SESSION['mensagens'])) {

		$_SESSION['mensagens'] = array();

	}

	$_SESSION['mensagens'][$msg] = $tipo;

}



function Mensagens(){

	$msgs = array();

	if (isset($_SESSION['mensagens'])) {

		$msgs = $_SESSION['mensagens'];

	}

	limpaMensagens();

	return $msgs;

}



function limpaMensagens(){

	if (@isset($_SESSION['mensagens'])) {

		unset($_SESSION['mensagens']);

	}

}


function mostraMensagem($style=""){

	$mensagens = "";

	if ($style != "") {

		$style = 'style="'.$style.'"';

	}

	$mens = array();

	$msgs = Mensagens();

	if (!empty($msgs)) {

		foreach ($msgs as $_mensagem => $tipo) {

			$mens[] = $_mensagem;

		}

		$mensagens = '<div class="'.$tipo.'" '.$style.'>'.implode("<br>\n", $mens).'</div>';

	}

	return $mensagens;

}



function goToPage($pagina){

	goToUrl($pagina);

}



function goToUrl($url){

	header("Location: ".$url);

	exit(0);

}



?>