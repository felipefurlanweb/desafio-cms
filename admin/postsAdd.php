<?php include_once 'header.php'; ?>
    
    <!-- CONTEUDO -->
    <div class="wrapper wrapper-content animated fadeInRight">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox">
                    <div class="ibox-title">
                        <h5>Adicionar Post</h5>
                    </div>
                    <div class="ibox-content">
                        <div class="row">
                            <form action="engine/formularios.php" method="POST" enctype="multipart/form-data">
                                <input type="hidden" name="id" value="postAdd">
                                <div class="col-xs-12 col-md-12">
                                    <div class="row">
                                        <div class="col-xs-12 col-md-12 form-group">
                                            <label>Título</label>
                                            <input type="text" class="form-control" name="titulo" required>
                                        </div>                                     
                                    </div>     
                                    <div class="row">
                                        <div class="col-xs-12 col-md-12 form-group">
                                            <label>Corpo</label>
                                            <textarea class="form-control" name="texto"></textarea>
                                        </div>                                     
                                    </div>          
                                    <div class="row">
                                        <div class="col-xs-12 col-md-12 form-group">
                                            <label>Path</label>
                                            <?php echo $pathURL; ?><input type="text" class="form-control" name="path">
                                        </div>                                     
                                    </div>                               
                                    <div class="row">
                                        <div class="col-xs-12 col-md-12 marginTop text-right">
                                            <hr>
                                            <a href="posts.php" class="btn btn-danger">Cancelar</a>
                                            <input type="submit" class="btn btn-primary" value="Salvar">
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- FIM CONTEUDO -->
    <script type="text/javascript">
        $(document).ready(function() {
            $(".path").keyup(function(event) {
                $("#spanPath").html($(".path").val());
            });
        });
    </script>
<?php include_once 'footer.php'; ?>
