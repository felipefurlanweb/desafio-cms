<?php 
if (!isset($_SESSION["cms"])) {
    session_start();
}
?>
<?php include_once 'engine/funcoes.php'; ?>
<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>Diagnostic Qualify | Login</title>

    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="font-awesome/css/font-awesome.css" rel="stylesheet">

    <link href="css/animate.css" rel="stylesheet">
    <link href="css/style.css" rel="stylesheet">
    <link href="css/custom.css" rel="stylesheet">

</head>

<body class="gray-bg">

    <div class="middle-box text-center loginscreen animated fadeInDown">
        <div>
            <h3>Bem-Vindo</h3>
            <p>Login in.</p>
            <?php echo mostraMensagem(); ?>
            <form class="m-t" role="form" method="POST" action="engine/formularios.php">
                <input type="hidden" name="id" value="login">
                <div class="form-group">
                    <input type="text" name="nome" class="form-control" placeholder="Usuário" required>
                </div>
                <div class="form-group">
                    <input type="password" name="senha" class="form-control" placeholder="Senha" required>
                </div>
                <button type="submit" class="btn btn-primary block full-width m-b">Login</button>
                <a href="signup.php" class="btn btn-info block full-width m-b">Criar conta</a>
            </form>
            <p class="m-t"> <small>&copy; 2017</small> </p>
        </div>
    </div>

    <!-- Mainly scripts -->
    <script src="js/jquery-3.1.1.min.js"></script>
    <script src="js/bootstrap.min.js"></script>

</body>

</html>
