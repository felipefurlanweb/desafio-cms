<?php include_once 'header.php'; ?>   

    <!-- CONTEUDO -->
    <div class="wrapper wrapper-content animated fadeInRight">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox">
                    <div class="ibox-title">
                        <h5>Posts</h5>
                    </div>
                    <?php echo mostraMensagem(); ?>
                    <div class="ibox-content">
                        <div class="row">
                            <div class="col-xs-12 col-md-12">
                                <a href="postsAdd.php" class="btn btn-primary">
                                    Adicionar
                                </a>
                            </div>
                            <div class="col-xs-12 col-md-12 marginTop">
                                <div class="table-responsive">
                                    <table class="table table-bordered">
                                        <thead>
                                            <tr>
                                                <th>Data</th>
                                                <th>Título</th>
                                                <th>Path</th>
                                                <th></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php 
                                                $query = "SELECT * FROM posts ORDER BY titulo ASC";
                                                $query = mysql_query($query);
                                                while ($res = mysql_fetch_array($query)) {
                                                    $id = $res["id"];                                                    
                                                    $titulo = $res["titulo"];
                                                    $data = $res["data"];
                                                    $path = $res["path"];
                                                    $data = FormataData_Brasil($data, "tsp");
                                                    echo
                                                    '
                                                        <tr>
                                                            <td>'.$data.'</td>
                                                            <td>'.$titulo.'</td>
                                                            <td>'.$path.'</td>
                                                            <td class="tdBtn text-center">
                                                                <a href="postsEdit.php?id='.$id.'" class="btn btn-info">
                                                                    <span class="glyphicon glyphicon-pencil"></span>
                                                                </a>
                                                                ';?>
                                                                <span class="btn btn-danger" onclick="delete_(<?php echo $id ?>)">
                                                                    <span class="glyphicon glyphicon-trash"></span>
                                                                </span>
                                                                <?php
                                                                echo
                                                                '
                                                            </td>
                                                        </tr>
                                                    ';
                                                }

                                            ?>

                                        </tbody>
                                    </table>    
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- FIM CONTEUDO -->
<script type="text/javascript">
        function delete_(id){
            swal({
              title: 'Tem certeza?',
              type: 'warning',
              showCancelButton: true,
              confirmButtonColor: '#1ab394',
              cancelButtonColor: '#d33',
              confirmButtonText: 'Sim',
              cancelButtonText: 'Cancelar'
            },
            function(isConfirm){
              if (isConfirm) {
                $.ajax({
                    url: 'engine/formularios.php',
                    type: 'POST',
                    dataType: 'html',
                    data: {id: 'postRemove', idModel: id},
                    success: function(res){
                        //swal("Sucesso!", "", "success");
                        window.location.href = window.location.href;
                    },
                    error: function(err){
                        swal("Ops", "Algo deu errado, tente novamente.", "error");
                        console.log(err);
                    }
                });
              }
            }
            );
        }

        $(document).ready(function(){
            $('table').DataTable({
                "language": {
                    "sEmptyTable": "Nenhum registro encontrado",
                    "sInfo": "Mostrando de _START_ até _END_ de _TOTAL_ registros",
                    "sInfoEmpty": "Mostrando 0 até 0 de 0 registros",
                    "sInfoFiltered": "(Filtrados de _MAX_ registros)",
                    "sInfoPostFix": "",
                    "sInfoThousands": ".",
                    "sLengthMenu": "_MENU_ resultados por página",
                    "sLoadingRecords": "Carregando...",
                    "sProcessing": "Processando...",
                    "sZeroRecords": "Nenhum registro encontrado",
                    "sSearch": "Pesquisar",
                    "oPaginate": {
                        "sNext": "Próximo",
                        "sPrevious": "Anterior",
                        "sFirst": "Primeiro",
                        "sLast": "Último"
                    },
                    "oAria": {
                        "sSortAscending": ": Ordenar colunas de forma ascendente",
                        "sSortDescending": ": Ordenar colunas de forma descendente"
                    }
                },
                responsive: true,
                dom: '<"html5buttons"B>lTfgitp',
                buttons: [
                    {extend: 'excel', title: 'ExampleFile'},
                    {extend: 'pdf', title: 'ExampleFile'}
                ]

            });

        });
</script>
<?php include_once 'footer.php'; ?>
