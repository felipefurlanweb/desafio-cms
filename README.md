<html>
<head>
	<title></title>
	<meta charset="utf-8">
</head>
<body>
	<p>Desafio CMS</p>
	<hr>
	<p><b>Configurações</b></p>
	<p>No diretório do projeto, existe um arquivo chamado: "banco.php", nele está as configurações de banco de dados.</p>
	<p>No diretório do projeto, existe um arquivo chamado: "header.php", nele está declarada a variável "$pathURL", que passa a url do projeto para que fique url amigável, exemplo desse projeto está assim: </p>
	<code>$path = 'http://'.$_SERVER['HTTP_HOST'].'/cms/';</code>
	<p>onde "cms" é a pasta raiz, alterar para a pasta raiz onde vai inserir o projeto</p>
	<p>No diretório do projeto, existe um arquivo chamado: "cms.sql", este arquivo contém a criação do banco e tabelas para o banco de dados do projeto</p>
	<hr>
	<p><b>Painel Administrativo</b></p>
	<p>Para acessar o Painel, acesse o diretório da pasta/admin</p>
	<hr>
	<p><b>Login</b></p>
	<p>Caso tenha seu cadastro, apenas preencha com seu usuário e senha, caso contrário, clique em criar conta, preencha os campos e clique em salvar, o sistema faz a verificação se existe um usuário com o mesmo que foi preenchido, caso tenha, uma mensagem é retornada, informando-o</p>
	<hr>
	<p><b>POSTS</b></p>
	<hr>
	<p><b>Como Adicionar um post no painel administrativo</b></p>
	<p>Clique no menu lateral esquerdo em Posts, após isso, clique em adicionar, digite o título, abaixo contém um campo para a descrição, é um editor de texto, pode inserir o que quiser dentro dele, abaixo contém o campo "Path", nele pode digitar uma path alternativa para acessar o post, caso não seja preenchida, a path será o ID do post.</p>
	<hr>
	<p><b>Como Editar um Post no painel administrativo</b></p>
	<hr>
	<p>Clique no menu lateral esquerdo em Post, após isso, clique no ícone de lápis em uma dos posts na tabela, preencha os campos que queira editar e clique em salvar</p>
	<hr>
	<p><b>Como Remover um Post no painel administrativo</b></p>
	<hr>
	<p>Clique no menu lateral esquerdo em Posts, após isso, clique no ícone de lixeira em uma dos posts na tabela e clique em sim</p>
	<hr>
</body>
</html>